import { Controller } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';

@Controller('user')
@ApiExcludeController()
export class UserController {}
