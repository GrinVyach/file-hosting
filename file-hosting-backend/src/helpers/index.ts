import { customAlphabet } from 'nanoid';

export const generateUniqueValue = (length: number) => {
  const nanoId = customAlphabet(
    'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghkmnpqrstuvwxyz123456789-',
    length,
  );

  return nanoId();
};
