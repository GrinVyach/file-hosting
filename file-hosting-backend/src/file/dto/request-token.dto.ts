import { IsOptional } from 'class-validator';

export class RequestTokenDto {
  @IsOptional()
  token?: string;
}
