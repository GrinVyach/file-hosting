import { PartialType } from '@nestjs/mapped-types';
import { CreateFileDto } from './create-file.dto';
import { IsDateString, IsOptional } from 'class-validator';

export class UpdateFileDto extends PartialType(CreateFileDto) {
  @IsDateString()
  @IsOptional()
  expDate?: string;

  @IsOptional()
  password?: string;
}
