export class TokenResponseDto {
  constructor(token: string) {
    this.token = token;
  }
  token: string;
}
