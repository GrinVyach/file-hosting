import { File } from '../entities/file.entity';

export class PathResponseDto {
  constructor(file: File) {
    this.publicPath = `${process.env.BASE_URL}/file/${file.id}`;
    this.id = file.id;
    this.sizeInBytes = file.size;
    this.createdAt = file.createdAt;
    this.expDate = file.expDate;
    this.realName = file.path;
  }

  realName: string;
  publicPath: string;
  id: string;
  sizeInBytes: number;
  createdAt: Date;
  expDate: Date;
}
