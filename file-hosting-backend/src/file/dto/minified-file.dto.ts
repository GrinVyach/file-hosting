import { File } from '../entities/file.entity';
import * as process from 'node:process';

export class MinifiedFileDto {
  constructor(file: File) {
    this.publicPath = `${process.env.BASE_URL}/file/${file.id}`;
    this.expDate = file.expDate;
    this.createdAt = file.createdAt;
    this.sizeInBytes = file.size;
    this.id = file.id;
    this.isSecured = file.password !== null;
    this.realName = file.path;
  }
  realName: string;
  publicPath: string;
  sizeInBytes: number;
  expDate: Date;
  id: string;
  createdAt: Date;
  isSecured: boolean;
}
