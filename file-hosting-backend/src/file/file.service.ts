import {
  Injectable,
  NotFoundException,
  StreamableFile,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { UpdateFileDto } from './dto/update-file.dto';
import { MinioService } from 'nestjs-minio-client';
import { Express, Request, Response } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from './entities/file.entity';
import { In, LessThan, Repository } from 'typeorm';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants';
import { addHours, differenceInDays } from 'date-fns';
import { ResponseDto } from './dto/response.dto';
import { TokenResponseDto } from './dto/token-response.dto';
import { PathResponseDto } from './dto/path-response.dto';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { User } from '../user/entities/user.entity';

@Injectable()
export class FileService {
  constructor(
    private readonly minioService: MinioService,
    private readonly configService: ConfigService,
    @InjectRepository(File)
    private readonly fileRepo: Repository<File>,
    private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}

  async initBucket() {
    const buckets = await this.minioService.client.listBuckets();
    if (buckets.length === 0) {
      await this.minioService.client.makeBucket('files');
    }
  }

  async findAll(req: Request, ip: string): Promise<any> {
    const user = await this.userService.findOneByIp(ip);
    return this.fileRepo.find({
      where: {
        user: {
          id: user.id,
        },
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async auth(id: string, password: string) {
    const file = await this.fileRepo.findOneBy({
      id,
    });

    if (!file) {
      throw new NotFoundException('Файл не найден');
    }

    const isMatch = await bcrypt.compare(password, file.password);
    if (!isMatch) {
      throw new UnauthorizedException('Неверный пароль');
    }

    return new TokenResponseDto(
      await this.jwtService.signAsync({
        id: file.id,
      }),
    );
  }

  async findOne(id: string, res: Response, ip: string, token?: string) {
    const file = await this.fileRepo.findOneBy({
      id,
    });

    if (!file) {
      throw new NotFoundException('Файл не найден');
    }

    if (file.password && !token) {
      return res.redirect(`${this.configService.get('FILE_VERIFY_URL')}/${id}`);
    }

    if (file.password) {
      try {
        const payload: File = await this.jwtService.verifyAsync(token, {
          secret: jwtConstants.secret,
        });

        if (payload.id !== file.id) {
          throw new UnauthorizedException('Некорретная авторизация');
        }
      } catch {
        return res.redirect(
          `${this.configService.get('FILE_VERIFY_URL')}/${id}`,
        );
      }
    }

    const buffer = await this.minioService.client.getObject(
      file.bucket,
      `${file.id}.${file.path.split('.').at(-1)}`,
    );

    const filename = Buffer.from(file.path, 'utf8').toString('latin1');

    res.set({
      'Content-Disposition': `attachment; filename="${filename}"`,
    });
    return new StreamableFile(buffer);
  }

  private async checkIsUserLimitExpired(user: User) {
    const userFiles = await this.fileRepo.find({
      where: {
        user: user,
      },
    });

    const allSizes = userFiles.map((file) => +file.size);
    if (allSizes.length === 0) return false;

    const size = allSizes.reduce((accum, next) => accum + next) / 2 ** 30;

    const maxSize = +this.configService.get('SIZE_LIMIT_PER_USER', 10);

    return size >= maxSize;
  }

  async uploadFile(
    file: Express.Multer.File,
    req: Request,
    ip: string,
  ): Promise<any> {
    await this.initBucket();
    if (!file || !file?.originalname || file?.size === 0) {
      throw new UnprocessableEntityException('Некорректный файл');
    }

    file.originalname = Buffer.from(file.originalname, 'latin1').toString(
      'utf8',
    );

    const user = await this.userService.findOneByIp(ip);

    if (await this.checkIsUserLimitExpired(user)) {
      throw new UnprocessableEntityException(
        'Вы исчерпали свой лимит на загрузку. Удалите старые файлы.',
      );
    }

    const fileEntity = await this.fileRepo.save(
      this.fileRepo.create({
        path: file.originalname,
        expDate: addHours(new Date(), 24 * 3 + 12), // 3.5 суток
        user: {
          id: user.id,
        },
        size: file.size,
      }),
    );

    await this.minioService.client.putObject(
      'files',
      `${fileEntity.id}.${file.originalname.split('.').at(-1)}`,
      file.buffer,
    );

    return new PathResponseDto(fileEntity);
  }
  async update(
    id: string,
    updateFileDto: UpdateFileDto,
    req: Request,
    ip: string,
  ) {
    const user = await this.userService.findOneByIp(ip);
    const file = await this.fileRepo.findOneBy({
      id,
      user: {
        id: user.id,
      },
    });

    if (!file) {
      throw new NotFoundException('Такого файла нет');
    }

    if (updateFileDto.password) {
      const salt = await bcrypt.genSalt();
      const password = updateFileDto.password;
      const hash = await bcrypt.hash(password, salt);

      await this.fileRepo.update(id, {
        password: hash,
      });
    }

    if (updateFileDto.expDate) {
      const diff = differenceInDays(
        new Date(updateFileDto.expDate),
        new Date(file.createdAt),
      );
      if (diff > 7 || diff < 0) {
        throw new UnprocessableEntityException(
          'Невозможно установить дату удаления дальше 7 дней после создания',
        );
      }
      await this.fileRepo.update(id, {
        expDate: new Date(updateFileDto.expDate),
      });
    }
    return new ResponseDto(true);
  }

  async remove(id: string, req: Request, ip: string) {
    const user = await this.userService.findOneByIp(ip);

    const fileToDelete = await this.fileRepo.findOneBy({
      id,
      user: {
        id: user.id,
      },
    });

    if (!fileToDelete) {
      throw new NotFoundException('Такого файла нет');
    }

    const path = `${id}.${fileToDelete.path.split('.').at(-1)}`;
    await this.minioService.client.removeObjects('files', [path]);

    await this.fileRepo.delete({
      id,
    });

    return new ResponseDto(true);
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async deleteExpiredFiles() {
    const files = await this.fileRepo.find({
      where: {
        expDate: LessThan(new Date()),
      },
    });

    await this.minioService.client.removeObjects(
      'files',
      files.map((file) => {
        return `${file.id}.${file.path.split('.').at(-1)}`;
      }),
    );

    await this.fileRepo.delete({
      id: In(files.map((file) => file.id)),
    });
  }
}
