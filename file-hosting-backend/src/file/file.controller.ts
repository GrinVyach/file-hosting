import {
  Body,
  Controller,
  Delete,
  Get,
  Ip,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  StreamableFile,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileService } from './file.service';
import { UpdateFileDto } from './dto/update-file.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Request, Response } from 'express';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { MinifiedFileDto } from './dto/minified-file.dto';
import { ResponseDto } from './dto/response.dto';
import { TokenResponseDto } from './dto/token-response.dto';
import { PathResponseDto } from './dto/path-response.dto';
import { RequestTokenDto } from './dto/request-token.dto';
import { Throttle } from '@nestjs/throttler';
import { PasswordDto } from './dto/password.dto';

@Controller('file')
@ApiTags('Файлы')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @ApiCreatedResponse({
    type: PathResponseDto,
    description: 'слова излишне...',
    isArray: false,
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiUnprocessableEntityResponse({
    description: 'какой-то кривой файл значит',
  })
  @Post('upload')
  @Throttle({ default: { limit: 10, ttl: 60000 } })
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Req() req: Request,
  ): Promise<PathResponseDto> {
    return this.fileService.uploadFile(
      file,
      req,
      req.header('x-real-ip') || req.ip,
    );
  }

  @ApiUnauthorizedResponse({
    description:
      'Если файл защищен паролем, то будет переадресас на http://localhost:8080/#/auth?fileId={id}\nЛибо если токен какой-то хуевый или уже прошло 60 сек то тоже пошлет',
  })
  @ApiOkResponse({
    description: 'Если не защищен паролем, то вернется файл тупа',
  })
  @ApiUnprocessableEntityResponse({
    description: 'Если вместо айди файла передана какая-то хуятина',
  })
  @Get(':id')
  findOne(
    @Param('id') id: string,
    @Res({ passthrough: true }) res: Response,
    @Query() query: RequestTokenDto,
    @Ip() ip,
  ): Promise<void | StreamableFile> {
    return this.fileService.findOne(id, res, ip, query.token);
  }

  @ApiOkResponse({
    type: TokenResponseDto,
    description:
      'Если введен верный пароль для файла, то вернется токен, который действует 60 секунд',
  })
  @ApiUnauthorizedResponse({
    description: 'Собсно вернется если пароль неверный',
  })
  @Post('auth/:id')
  authFile(
    @Param('id') id: string,
    @Body() body: PasswordDto,
  ): Promise<TokenResponseDto> {
    return this.fileService.auth(id, body.password);
  }

  @ApiOkResponse({
    type: MinifiedFileDto,
    isArray: true,
    description: 'слова излишне...',
  })
  @Get('')
  async getAll(@Req() req: Request): Promise<MinifiedFileDto> {
    const files = await this.fileService.findAll(
      req,
      req.header('x-real-ip') || req.ip,
    );
    return files.map((file) => new MinifiedFileDto(file));
  }

  // done
  @ApiOkResponse({
    type: ResponseDto,
    description: 'Если всё ок, вернется просто success true',
  })
  @ApiUnauthorizedResponse({
    description:
      'Если пытаешься редачить не свой файл (дада.... большой брат видит айпишнеки...)',
  })
  @ApiUnprocessableEntityResponse({
    description: 'Нельзя устанавливать дату дальше 7 дней после создания',
  })
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFileDto: UpdateFileDto,
    @Req() req: Request,
  ): Promise<ResponseDto> {
    return this.fileService.update(
      id,
      updateFileDto,
      req,
      req.header('x-real-ip') || req.ip,
    );
  }

  @ApiOkResponse({
    type: ResponseDto,
    description: 'Если файл удалился, то он удалился (с) миньон-качок',
  })
  @ApiUnauthorizedResponse({
    description: 'Не удаляйте не свои файлы!!!!!!!!!!',
  })
  @Delete(':id')
  remove(@Param('id') id: string, @Req() req: Request) {
    return this.fileService.remove(id, req, req.header('x-real-ip') || req.ip);
  }
}
