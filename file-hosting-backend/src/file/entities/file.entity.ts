import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../user/entities/user.entity';
import { generateUniqueValue } from '../../helpers';

@Entity()
export class File {
  @PrimaryColumn('varchar', {
    length: 6,
  })
  id: string;

  @BeforeInsert()
  setId() {
    this.id = generateUniqueValue(6);
  }

  @Column({ type: 'varchar', default: 'files' })
  bucket: string;

  @Column({ type: 'varchar', nullable: false })
  path: string;

  @Column({ type: 'varchar', nullable: true })
  password: string;

  @Column({ type: 'timestamp', nullable: true })
  expDate: Date;

  @Column({ type: 'bigint', unsigned: true })
  size: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => User, (user) => user.files)
  user: User;
}
