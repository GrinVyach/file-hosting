import {defineStore} from 'pinia'
import {UploadedFile} from "../api/responses";
import {Ref, ref, UnwrapRef, watch} from "vue";
import {router} from "../router";

export const useLoadedFileStore = defineStore('loadedFile', () => {
    const uploadedFile: Ref<UnwrapRef<UploadedFile|null>> = ref(null)
    const setUploadedFile = (file: UploadedFile) => {
        uploadedFile.value = file
    }

    watch(uploadedFile, async (newFile) => {
        if(newFile !== null) {
            await router.push('success')
        }
    })

    return { uploadedFile, setUploadedFile }
})
export const useOverlayStore = defineStore('overlay', () => {
    const overlayVisible = ref(false)
    const qrUrl = ref('')

    const setOverlayVisibility = (status: boolean) => {
        overlayVisible.value = status
    }
    const setOverlayUrl = (url: string) => {
        qrUrl.value = url
    }

    return { overlayVisible, qrUrl, setOverlayVisibility, setOverlayUrl }
})