import axios, {AxiosProgressEvent, AxiosRequestConfig, AxiosResponse} from 'axios';
import {PatchFileRequest} from "./requests";
import {FileItem, ResponseWrap, UploadedFile} from "./responses";
import {showToast} from "../helpers";


const api = axios.create({
    baseURL: import.meta.env.VITE_BACKEND_URL,
})


api.interceptors.response.use((response) => response, (error) => {
    showToast({ severity: 'error', summary: 'Произошла ошибка!', detail: error.response.data.message || 'Ошибка сервера', life: 3500 })
})

const isZip = (file: File | Blob) => {
    return file instanceof Blob && !(file instanceof File)
}

const getZipName = () => {
    const date = new Date().getTime()
    return `${date}.zip`
}

export const uploadFile = async (file: File, onProgressCallback: (progressEvent: AxiosProgressEvent) => void): Promise<ResponseWrap<UploadedFile>> => {

    const formData = new FormData();

    const filename = isZip(file) ? getZipName() : file.name
    formData.append("file", file, filename);

    const config: AxiosRequestConfig = {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        onUploadProgress: progressEvent => onProgressCallback(progressEvent)
    }

    const response: AxiosResponse<UploadedFile> = await api.post('file/upload', formData, config)
    return {
        statusCode: response.status,
        data: response.data
    }
}

export const patchFile = async (fileId: string, data: PatchFileRequest) => {
    return await api.patch(`file/${fileId}`, data)
}
export const deleteFile = async (fileId: string) => {
    return await api.delete(`file/${fileId}`)
}

export const auth = async (fileId: string, password: string) => {
    return await api.post(`file/auth/${fileId}`, {
        password
    })
}


export const getMyFiles = async (): Promise<ResponseWrap<FileItem[]>> => {

    const response: AxiosResponse<FileItem[]> = await api.get(`file`)
    return {
        statusCode: response.status,
        data: response.data
    }
}
