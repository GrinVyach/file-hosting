export type ResponseWrap<T> = {
    statusCode: number;
    data: T;
}

export type ErrorResponse = {
    status: number,
    message: string
}

export type UploadedFile = {
    publicPath: string
    id: string
    sizeInBytes: number
    createdAt: Date | string
    expDate: Date | string
    isSecured: boolean,
    realName: string
}

export type Success = {
    success: boolean
}

export type Token = {
    token: string
}

export type FileItem = UploadedFile & {
    isSecured: boolean
}