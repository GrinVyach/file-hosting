export type PatchFileRequest = {
    expDate?: Date | string,
    password?: string
}