import {createRouter, createWebHistory} from 'vue-router'
import HomeView from "../components/views/HomeView.vue";
import UploadedFileView from "../components/views/UploadedFileView.vue";
import VerifyView from "../components/views/VerifyView.vue";


const routes = [
    { path: '/', name: 'home', component: HomeView },
    { path: '/success', name: 'success', component: UploadedFileView },
    { path: '/verify/:id', name: 'verify', component: VerifyView },
]

export const router = createRouter({
    history: createWebHistory(),
    routes,
})