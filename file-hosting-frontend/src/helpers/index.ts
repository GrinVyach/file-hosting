export const isMobile = () =>  {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true
    } else {
        return false
    }
}
import { getCurrentInstance } from 'vue';
import { useToast } from 'primevue/usetoast';

let toast: any;

export const initToast = (app: any) => {
    toast = app.config.globalProperties.$toast;
};

export const showToast = (options: any) => {
    if (!toast) {
        const instance = getCurrentInstance();
        if (instance) {
            toast = useToast();
        } else {
            throw new Error('No PrimeVue Toast provided!');
        }
    }
    toast.add(options);
};

export const numWord = (value: number, words: string[]) => {
    value = Math.abs(value) % 100;
    const num = value % 10;
    if(value > 10 && value < 20) return words[2];
    if(num > 1 && num < 5) return words[1];
    if(num == 1) return words[0];
    return words[2];
}

