import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import VueSvgInlinePlugin from "vue-svg-inline-plugin";
import "vue-svg-inline-plugin/src/polyfills";
import {router} from "./router";
import {createPinia} from "pinia";
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import 'primevue/resources/themes/aura-light-green/theme.css'
import {initToast} from "./helpers";

import JSZip from 'jszip'


const app = createApp(App);
app.use(JSZip)
app.use(router)
app.use(PrimeVue);
app.use(ToastService);
initToast(app);

const pinia = createPinia()
app.use(pinia)

app.use(VueSvgInlinePlugin, {
    attributes: {
        data: [ "src" ],
        remove: [ "alt" ]
    }
});

app.mount("#app");

export default app