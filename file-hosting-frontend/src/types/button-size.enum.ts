export enum ButtonSize {
    BIG = 'big',
    MEDIUM = 'medium',
    REGULAR = 'regular',
}