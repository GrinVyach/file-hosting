export enum ButtonType {
    DANGER = 'danger',
    INFO = 'info',
    SUCCESS = 'success',
}